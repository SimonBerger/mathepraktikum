/***********************************************************
*  Name       : algo.h                                     *
*  Verwendung : Verschiedene String Matching Algorithmen   *
*  Autor      :                                            *
*  Datum      :                                            *
*  Sprache    : C++                                        *
***********************************************************/

/**
* \brief Basis-Klasse die einen String Matching Algorithmus repraesentiert. Wichtig: Die Einzelnen Algorithmen werden von dieser Klasse abgeleitet.
*/
class SMAP
{
	public:
		/**
		* \brief Fuert die Suche nach einem Pattern im Text durch.
		* \param s Der Text in dem gesucht werden soll.
		* \param p Das Pattern nach dem gesucht werden soll.
		* \param result Vektor in dem die Fundstellen gespeichert werden sollen.
		*/
		virtual void find_single(const string &s, const string &p, vector<int> &result) = 0;
		/**
		* \brief Fuert die Suche nach mehreren Pattern im Text durch.
		* \param s Der Text in dem gesucht werden soll.
		* \param p Die Pattern nach denen gesucht werden soll.
		* \param result Vektor in dem die Fundstellen gespeichert werden sollen.
		*/
		virtual void find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result) = 0;
	protected:
		string name;
		SMAP():name("SMAP"){}
		SMAP(string s):name(s){}
};

class Simple : public SMAP
{
	public:
		void find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result);
	protected:
		Simple(string s):SMAP(s){}
};

class Naiv : public Simple
{
	public:
		Naiv():Simple("Naiv"){}
		void find_single(const string &s, const string &p, vector<int> &result);
};

class KnuthMorrisPratt : public Simple
{
	private:
		void function(const string &p, vector<int>& f);
	public:
		KnuthMorrisPratt():Simple("KnuthMorrisPratt"){}
		void find_single(const string &s, const string &p, vector<int> &result);
};

class BoyerMoore : public Simple
{
	private:
		void function(const string &p, vector<int>& f);
	public:
		BoyerMoore():Simple("BoyerMoore"){}
		void find_single(const string &s, const string &p, vector<int> &result);
};

template<class H>
class RabinKarp : public SMAP
{
	public:
		RabinKarp():SMAP("RabinKarp"){name.append(H::name);}
		void find_single(const string &s, const string &p, vector<int> &result);
		void find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result);
};


void Simple::find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result)
{
	start_multiple();
	
	for(int i = 0; i < p.size(); i++){
		vector<int> res;
		find_single(s,p[i],res);
		result.push_back(res);
	}

	finish_multiple(name, s, p, result);
}

void Naiv::find_single(const string &s, const string &p, vector<int> &result)
{
	start();

	int j;
	int k = p.length();
	int n = s.length();

	for(int i = 0; i+k <= n; i++){
		bool equal = true;
		for(int j = 0; j < k && equal; j++){
			equal &= compare(s[i+j], p[j]);
		}
		if(equal){
			result.push_back(i);
		}
	}

	finish(name);
}

void KnuthMorrisPratt::function(const string& p, vector<int>& f){
	f[0] = -1;
	for(int j = 1; j < p.length(); j++){
		int l = 0;
		while(2*l < j){
			if(l != j-l-1 && compare(p[l], p[j-l-1])){
				l++;
			} else {
				break;
			}
		}
		f[j] = l;
	}
}

void KnuthMorrisPratt::find_single(const string &s, const string &p, vector<int> &result)
{
	start();

	vector<int> f(p.length());
	function(p,f);

	int j = 1;
	int k = p.length();
	int n = s.length();
	bool ungleich;

	for(int i = 0; i+k-f[j-1] < n-k; i = i + (j-1)-f[j-1]){

		j = f[j-1];
		if(j < 0){
			j = 0;
		}
		bool equal = true;
		for(; j<k && equal; j++){
			equal &= compare(s[i+j], p[j]);
		}
		if(equal){
			result.push_back(i);
		}
		
	}
	int i = n-k;
	bool equal = true;
	for(int l = 0; l < k && equal; l++){
		equal &= compare(s[i+l], p[l]);
	}
	if(equal){
		result.push_back(i);
	}


	finish(name);
}

void BoyerMoore::function(const string& p, vector<int>& f){
	f[0] = 1;
	signed int patLen = p.length();
	f[patLen] = 1;

	for(int index = 1; index < p.length(); index++){	//index ist die Anzahl erfolgreicher Vergleiche
		int firstRule = 1;		//Gibt Shift nach erster Regel an
		int secondRule = 1;		//Gibt Shift nach zweiter Regel an
		
		for(int i = 0; i < patLen-index; i++){
			if(compare(p.at(i), p[patLen-index])){
				firstRule = patLen-i-1;
			}
		}

		string word = p.substr(p.length()-index);
		signed int leftBorder = -index+1;

		for(signed int i = leftBorder; i+index < patLen; i++){
			int compareLength = std::min(index, i+index);
			string compareWord = p.substr(std::max(0,i), compareLength);

			bool equal = true;
			for(int j = 0; j < compareLength && equal; j++){
				equal &= compare(compareWord[j], word[index-compareLength+j]);
			}
			if(equal){
				secondRule = index - (i+compareLength);
			}
		}

		f[index]= std::max(firstRule, secondRule);
	}
	
}

void BoyerMoore::find_single(const string &s, const string &p, vector<int> &result)
{
	start();

	vector<int> f(p.length()+1);
	function(p,f);

	int i = p.length()-1;
	while(i <= s.length()-1){
		int len = 0;

		while(len < p.length() && compare(s[i-len], p[p.length()-1-len])){
			len++;
		}
		if(len == p.length()){
			result.push_back(i - p.length()+1);
		}
		if(i < s.length()-1){
			i += f[len];
			if(i > s.length()-1){
				i = s.length()-1;
			}
		} else {
			i++;
		}
	}

	finish(name);
}

template<class H>
void RabinKarp<H>::find_single(const string &s, const string &p, vector<int> &result)
{
	start();

	vector<string> pp;
	pp.push_back(p);
	vector<vector<int> > rresult;

	find_multiple(s,pp,rresult);
	result = rresult[0];

	finish(name);
}

template<class H>
void RabinKarp<H>::find_multiple(const string &s, const vector<string> &p, vector<vector<int> > &result)
{
	start_multiple();
	start();

	int len = p[0].length();

	for(int i = 0; i < p.size(); i++){
		vector<int> res;
		result.push_back(res);
		if(p[i].length() < len){
			len = p[i].length();
		}
	}

	H Hasher;
	Hasher.init(len);

	for(int i = 0; i < p.size(); i++){
		Hasher.set(p[i]);
		Hasher.save(i);
	}

	Hasher.set(s);
	for(int i = 0; i+len <= s.length(); i++){
		HashList* l = Hasher.find();
		while(l != NULL){
			if(p[l->value].length() + i <= s.length()){
				bool equal = true;
				for(int j = 0; j < p[l->value].length() && equal; j++){
					equal &= compare((p[l->value])[j], s[i+j]);
				}
				if(equal){
					result[l->value].push_back(i);
				}
			}
			l = l->next;
		}
		Hasher.roll(s[i], s[i+len]);
	}

	finish(name);
	finish_multiple(name, s,p, result);
}

