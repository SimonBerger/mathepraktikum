/***********************************************************
*  Name       : hash.h                                     *
*  Verwendung : Verschiedene Hash-Funktionen               *
*  Autor      :                                            *
*  Datum      :                                            *
*  Sprache    : C++                                        *
***********************************************************/

#include <cmath>

using namespace std;

/**
* \brief Klasse die das Arbeiten mit Hash-Funktionen beinhaltet. Sie beinhaltet den Hash-Wert, eine Hash-Tabelle und fuert auch die Suche nach einem Hash-Wert durch.
* \param val_hash Der aktuell betrachtete Hash-Wert
* \param len Laenge der Woerter von denen der Hash-Wert berechnet werden kann
* \param table Die Hash-Tabelle. Speichert zu jedem Hash-Wert eine Liste vom Typ HashList. $table[i]$ soll die Liste aller Pattern sein, die den Hash-Wert $i$ haben.
* \param name Name der Hash-Funktion.
*/
class RollingHash
{
	public:
		/**
		* \brief Fuehrt den roll-Algorithmus durch. Der alte Hash-Wert ist val_hash und das Ergebnis wird ebenfalls in val_hash gespeichert.
		* \param del Buchstabe der aus dem Hash-Wert geloescht werden soll
		* \param add Buchstabe der zu dem Hash-Wert hinzugefuegt werden soll
		*/
		virtual void roll(const char &del, const char &add) = 0;
		/**
		* \brief Berechnet den Hash-Wert fuer einen gegebenen String.
		* \param s String dessen Hash-Wert berechnet werden soll
		*/
		virtual void set(const string &s) = 0;
		/**
		* \brief Setzt val_hash zurueck auf 0.
		*/
		void del();
		/**
		* \brief Speichert in der Hash-Tabelle an der Stelle val_hash den Wert v ab
		* \param v Wert der abgespeichert werden soll
		*/
		void save(int val);
		virtual void init(int m) = 0;
		/**
		* \brief Gibt die HashList zurueck, die in der Hash-Tabelle an der Stelle val_hash gespeichert ist.
		*/
		HashList* find();
		/**
		* \brief Gibt den aktuellen Hash-Wert zurueck, also val_hash.
		*/
		int value();

		unsigned char mod(long num1, char num2);

	protected:
		int val_hash;
		int len;
		vector<HashList*> table;
		static string name;

};


void RollingHash::save(int v)
{
	if(table[val_hash]!=NULL)
		table[val_hash]->add(v);
	else
		table[val_hash] = new HashList(v);
}

HashList* RollingHash::find()
{
	return table[val_hash];
}

void RollingHash::del()
{
	val_hash=0;
}

int RollingHash::value()
{
	return val_hash;
}

unsigned char RollingHash::mod(long a, char b)
{ 
	return (a%b+b)%b; 
}





class HashSimple : public RollingHash
{
	public:
		HashSimple(int m=0);
		HashSimple(int m, const string &s);
		void roll(const char &del, const char &add);
		void set(const string &s);
		void init(int m);
		static string name;
};
string HashSimple::name = "HashSimple";

void HashSimple::init(int m)
{
	val_hash = 0;
	len = m;
	table = vector<HashList*>(m*UCHAR_MAX);
}

void HashSimple::roll(const char &del, const char& add)
{
	if(del != add){
		unsigned char delValue = del;
		unsigned char addValue = add;
		val_hash = val_hash - delValue + addValue;
	}

}
HashSimple::HashSimple(int m)
{
	init(m);
}

HashSimple::HashSimple(int m, const string &s)
{
	init(m);
	set(s);
}

void HashSimple::set(const string &s)
{
	int sum = 0;
	for(int i = 0; i < len; i++){
		unsigned char wi = s.at(i);
		sum += wi;
	}
	val_hash = sum;
}


class HashBetter : public RollingHash
{
	public:
		HashBetter(int m=0);
		HashBetter(int m, const string &s);
		void roll(const char &del, const char &add);
		void set(const string &s);
		void init(int m);
		unsigned char ModularPower(unsigned char base, int exponent, unsigned char mod);
		static string name;
	private:
		int a;
		int z;
		int q;
};

string HashBetter::name = "HashBetter";

void HashBetter::init(int m)
{
	val_hash = 0;
	len = m;
	a = 256;
	z = 103;

	q = ModularPower(a,len,z);

	table = vector<HashList*>(z);
}

void HashBetter::roll(const char &del, const char& add)
{
	
	unsigned char deleteValue = del;
	unsigned char addValue = add;

	long temp = mod(q*deleteValue, z);

	unsigned char newValue = mod(val_hash*a + addValue + z - temp, z);
	val_hash = newValue;

}

HashBetter::HashBetter(int m)
{
	init(m);
}

HashBetter::HashBetter(int m, const string &s)
{
	init(m);
	set(s);
}

unsigned char HashBetter::ModularPower(unsigned char base, int exponent, unsigned char modular){
	unsigned char res = 1;
	for(int i = 0; i < exponent; i++){
		res = mod(res*a, modular);
	}
	return res;
}

void HashBetter::set(const string &s)
{
	int sum = 0;
	for(int i = 1; i <= len; i++){
		unsigned char wi = s.at(i-1);
		unsigned char factor = ModularPower(a, len-i, z);
		unsigned char summand = mod(wi*factor, z);

		sum = mod(sum + summand, z);
	}
	val_hash = sum;
}


